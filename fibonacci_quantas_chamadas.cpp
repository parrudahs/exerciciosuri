#include <iostream>

using namespace std;

int fibonacci(int n)
{
	if(n == 0)
		return 0;
	if(n == 1)
		return 1;
	return fibonacci(n -1) + fibonacci(n - 2);
}

int chamadas(int n)
{
   	if(n == 0 || n == 1)
        return 1;
    return 1 + chamadas(n-1) + chamadas(n-2);
}

int main()
{
	int testes, numero;
	cin >> testes;

	for(int i=0; i < testes; i++){
		cin >> numero;
		cout << "fib("<<numero<<") = "<< chamadas(numero)-1 <<  " calls = "<< fibonacci(numero)<< endl;
	}

	return 0;
}
