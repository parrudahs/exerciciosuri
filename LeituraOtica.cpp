#include <iostream>

using namespace std;

char resultado(int valor){
	switch(valor){
		case 0:
			return 'A';
		case 1:
			return 'B';
		case 2:
			return 'C';
		case 3:
			return 'D';
		case 4:
			return 'E';
		case -1:
			return '*';
	}

}

int main()
{
	int alternativas[5];
	int casosDeTestes;
	int repete = 0;
	int indice = 0;
	int result;
	while(cin >> casosDeTestes){
		if(casosDeTestes == 0)
			break;
		for(int i = 0; i < casosDeTestes; i++){
			for(int j = 0; j < 5; j++){
				cin >> alternativas[j];
				if(alternativas[j] <= 127){
					repete += 1;
					indice = j;
				}
			}
			if(repete > 1 || repete == 0){
				result = -1;
			}else{
				result = indice;
			}
			cout << resultado(result) << endl;
			indice = 0;
			repete = 0;			
		}
	}
	return 0;
}
