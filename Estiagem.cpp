#include <iostream>
#include <map>
#include <iomanip>

using namespace std;

int main()
{
	long int numero;
	int pessoas, consumo;
	int tPessoas = 0, tConsumo = 0;
	double result = 0;
	int cont = 1;

	map<int, int> mapa;
	map<int, int>:: iterator it;
	
	cin >> numero;
	while(numero != 0){

		for(long int i =0; i < numero; i++){
			cin >> pessoas >> consumo;
			tPessoas += pessoas;
			tConsumo += consumo;
			mapa[consumo/pessoas] += pessoas;
		}

		cout << "Cidade# " << cont <<":" << endl;
		for(it = mapa.begin(); it != mapa.end(); it++){
			if(it != mapa.begin()) cout << " ";
			cout << it->second << "-" << it->first;
		}
		cout << endl;
		result = ((tConsumo*1.0)/(tPessoas*1.0) - 0.0049999999);
		cout << fixed << setprecision(2);
		cout << "Consumo medio: "<< result <<" m3." << endl;		
				
		tPessoas = 0;
		tConsumo = 0;
		result = 0;
		mapa.clear();		
		cin >> numero;
		cont++;
		if(numero != 0)
			cout << endl;
	}
	return 0;
}
