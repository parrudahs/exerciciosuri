#include <iostream>
#include <string>
#include <stack>
#include <queue>

using namespace std;

int main()
{
	int casosDeTestes = 0;
	string infixa;
	string resultado;
	int c = 0;

	cin >> casosDeTestes;

	stack<char> pilha;
	queue<char> result;

	while(casosDeTestes > 0)
	{
		cin >> infixa;

		for(int i=0; i < infixa.size(); i++){
			if(infixa[i] == '*' || infixa[i] == '+' || infixa[i] == '/' ||
			   infixa[i] == '^' || infixa[i] == '-' || infixa[i] == ')' || infixa[i] == '(')
			{
				if(pilha.empty()){
					pilha.push(infixa[i]);
					cout << "empilhar I = " << infixa[i] << endl;
				}else{
					cout << "emfilheira = " << pilha.top() << endl;
					result.push(pilha.top());
					cout << "desempilhar = " << pilha.top() << endl;
					pilha.pop();
				}
			} else {
				result.push(infixa[i]);
				cout << "emfilheira a = " << infixa[i] << endl;
			}
		}
		while(!pilha.empty()){
			result.push(pilha.top());
			cout << "emfilheira = " << pilha.top() << endl;
			cout << "desempilhar = " << pilha.top() << endl;
			pilha.pop();
			
		}
		casosDeTestes--;
	}
	return 0;
}
