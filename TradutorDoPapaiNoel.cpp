#include <iostream>
#include <string>
#include <map>

using namespace std;

int main()
{
	string busca;
	map<string, string> mensagem;

	mensagem["brasil"] =  "Feliz Natal!";
	mensagem["alemanha"] =  "Frohliche Weihnachten!";
	mensagem["austria"] =  "Frohe Weihnacht!";
	mensagem["coreia"] =  "Chuk Sung Tan!";
	mensagem["espanha"] =  "Feliz Navidad!";
	mensagem["grecia"] =  "Kala Christougena!";
	mensagem["estados-unidos"] =  "Merry Christmas!";
	mensagem["inglaterra"] =  "Merry Christmas!";
	mensagem["australia"] =  "Merry Christmas!";
	mensagem["portugal"] =  "Feliz Natal!";
	mensagem["suecia"] =  "God Jul!";
	mensagem["turquia"] =  "Mutlu Noeller";
	mensagem["argentina"] =  "Feliz Navidad!";
	mensagem["chile"] =  "Feliz Navidad!";
	mensagem["mexico"] =  "Feliz Navidad!";
	mensagem["antardida"] =  "Merry Christmas!";
	mensagem["canada"] =  "Merry Christmas!";
	mensagem["irlanda"] =  "Nollaig Shona Dhuit!";
	mensagem["belgica"] =  "Zalig Kerstfeest!";
	mensagem["italia"] =  "Buon Natale!";
	mensagem["libia"] =  "Buon Natale!";
	mensagem["siria"] =  "Milad Mubarak!";
	mensagem["marrocos"] =  "Milad Mubarak!";
	mensagem["japao"] =  "Merii Kurisumasu!";

	while(getline(cin, busca) != NULL){
		if(mensagem.find(busca) == mensagem.end())
			cout << "--- NOT FOUND ---" << endl;
		else
			cout << mensagem[busca] << endl;
	}

	return 0;
}
