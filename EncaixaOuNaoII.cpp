#include <iostream>
#include <string>

using namespace std;

int main()
{
	int testes;
	string numeros;
	string finalNumero;
	string copia;
	bool seque =  true;
 
	cin >> testes;

	for(int i = 0; i < testes; i++){
		cin >> numeros >> finalNumero;
		if(finalNumero.size() <= numeros.size()){
			int ondeComeca = numeros.size() - finalNumero.size();
			for(int j = ondeComeca, k = 0; j < numeros.size(); j++, k++){
				if(finalNumero[k] != numeros[j]){
					seque = false;
					break;
				}
			}
		}else{
			seque = false;
		}
		
		if(seque)
			cout << "encaixa" << endl;
		else
			cout << "nao encaixa" << endl;

		seque = true;
	}
	return 0;
}
