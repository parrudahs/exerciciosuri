#include <iostream>
#include <string>

using namespace std;

int numeroAscii(char letra)
{
	return letra;
}

string inverte(string palavra, int recuo){
	char numero = 0;
	for(int i =0; i < palavra.length(); i++){
		if(numeroAscii(palavra[i])-recuo < 65){
			numero = numeroAscii(palavra[i]) + 25 - (recuo -1);
		}else{
			numero = numeroAscii(palavra[i]) - recuo;
		}
		palavra[i] = numero;			
	}
	return palavra;
}


int main(){
	int n;
	string palavra;
	int recuo = 0;
	cin >> n;
	while(n > 0){
		cin >> palavra;
		cin >> recuo;
		cout << inverte(palavra, recuo) << endl;
		n--;
	}
	return 0;
}










