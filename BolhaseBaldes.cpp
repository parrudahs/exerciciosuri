#include <iostream>
#include <string>

using namespace std;

string result(int numeros[], int tamanho){
	int n = 0;
	int i = (tamanho - 1) / 2;
	int chave, k, aux; 
	while(i != 0)
	{
	  do
	  {
		 chave = 1;
		 for(k = 0; k < tamanho - i; ++k)
		 {
		    if(numeros[k] > numeros[k + i])
		    {
		       aux = numeros[k];
		       numeros[k] = numeros[k + i];
		       numeros[k + i] = aux;
		       chave = 0;
				n++;
		    }
		 }
	  }while(chave == 0);
	  i = i / 2;
	}
	if(n % 2 == 0)
		return "Carlos";
	return "Marcelo";
}



int main()
{
	int n;
	while(true){
		cin >> n;
		if(n == 0)
			break;
		int numeros[n];
		for(int i = 0; i < n; i++)	
			cin >> numeros[i];

		cout << result(numeros, n) << endl;
		
	}
	return 0;
}
