#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int n, q, t, v;
	int cont = 1;

	while(true){

		cin >> n >> q;
		if(n == 0 && q == 0)
			break;
		
		vector<int> marmores;
		vector<int>::iterator it;

		for(int i = 0; i < n; i++){
			cin >> t;
			marmores.push_back(t);
		}
	
		sort(marmores.begin(), marmores.end());

		cout << "CASE# "<< cont <<":" << endl;
		for(int i = 0; i < q; i++){
			cin >> v;
			it = find(marmores.begin(), marmores.end(), v);
			if(it != marmores.end()){
				int posicao = distance(marmores.begin(), it);
				cout << v <<" found at "<< posicao+1 << endl;
			}else{
				cout << v <<" not found"<< endl;
			}
		}				
	
		cont++;
	}
	return 0;
}
