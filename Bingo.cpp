#include <iostream>
#include <set>
#include <math.h>

using namespace std;

int main()
{
	int N,B;
	while(true){

		cin >> N >> B;

		if(N == 0 && B == 0)
			break;
		
		int bolas[B];		

		for(int i=0; i < B; i++)
			cin >> bolas[i];

		set<int> diferenca;

		for(int i = 0; i < B; i++){
			for(int j = i; j < B; j++)
				diferenca.insert(fabs(bolas[i] - bolas[j]));
		}

		if(diferenca.size() == (N+1))
			cout << "Y" << endl;
		else
			cout << "N" << endl;

	}

	return 0;
}
