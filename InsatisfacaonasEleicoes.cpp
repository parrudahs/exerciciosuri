#include <iostream>
#include <map>

using namespace std;
 
double porcetagem(double valor, int total)
{
    return valor * 100 / total;
}
 
int main(){
    int casosDeTestes = 0;
    int n, m;

 	map<int, double> mapVotos;
	map<int, double>::iterator it;

	int voto;
	int contador = 0;
	int result = -1;

    cin >> casosDeTestes;
 
    for(int i = 0; i < casosDeTestes; i++){
        cin >> n >> m;	
	
		for(int j=0; j < m; j++){
			cin >> voto;
			mapVotos[voto]++;
		}

		for(it = mapVotos.begin(); it != mapVotos.end(); it++)
		{
			if(porcetagem( it->second , m) > 50){
				result = it->first;
				break;
			}
		}
		cout << result << endl;
		result = -1; 
		mapVotos.clear();
    }
 
    return 0;
}
