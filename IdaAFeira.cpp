#include <iomanip> 	
#include <iostream>
#include <string>
#include <map>

using namespace std;

int main()
{
	int casosDeTestes, m, p;

	map<string, float> produtos;
	map<string, float>::iterator it;
	
	string produto;
	float valor = 0;
	float total = 0;
	
	cin >> casosDeTestes;

	while(casosDeTestes > 0){
		cin >> m;
		for(int i=0; i < m; i++){
			cin >> produto >> valor;
			produtos[produto] = valor;
		}
		cin >> p;
		for(int i = 0; i < p; i++){
			cin >> produto >> valor;
			it = produtos.find(produto);
			if(it != produtos.end())
				total += valor * it->second;				
		}
		cout <<  "R$ " << setiosflags (ios::fixed) << setprecision (2) << total << endl; 
		total = 0;
		produtos.clear();
		casosDeTestes--;
	}
	return 0;
}
