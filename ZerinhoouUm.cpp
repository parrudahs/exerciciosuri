#include <iostream>
#include <string>

using namespace std;

char result(int A, int B, int C)
{	
	if(A == B && B == C)
		return '*';
	else if(A == B && B != C)
		return 'C';
	else if(A != B && B == C)
		return 'A';
	else
		return 'B';
}

int main()
{
	int A, B, C;
	while(true){
		cin >> A >> B >> C;
		if(!cin)
			break;
		cout << result(A,B,C) << endl;
	}
	return 0;
}
