#include <iostream>
#include <iomanip> 	

using namespace std;

int main()
{
	int testes;

	int qTurma = 0;
	float somatorio = 0;
	float Media = 0;
	int cont = 0;

	cin >> testes;

	while(true)
	{
		if(testes == 0)
			break;
		
		cin >> qTurma;

		int turma[qTurma];

		for(int i = 0; i < qTurma; i++){
			cin >> turma[i];
			somatorio += turma[i];
		}
		Media = somatorio / qTurma;
		for(int i = 0; i < qTurma; i++){
			if(turma[i] > Media)
				cont++;	
		}
		Media = (float)(cont * 100) / qTurma;
		cout << setiosflags (ios::fixed) << setprecision (3) <<Media<<"%"<< endl;

		Media = 0;	
		cont = 0;	
		somatorio = 0;

		testes--;
	}
	return 0;
}
