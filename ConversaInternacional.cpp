#include <iostream>
#include <string>

using namespace std;

string result(string idiomas[], int tamanho)
{
	string idioma; 
	for(int i =1; i < tamanho; i++){
		if(idiomas[i] != idiomas[i-1]){
			idioma = "ingles";
			break;
		}
		idioma = idiomas[i];		
	}
	return idioma;
}

int main()
{
	int casoTestes = 0;
	int pessoasGrupo = 0;
	cin >> casoTestes;
	for(int i=0; i < casoTestes; i++){
		cin >> pessoasGrupo ;
		string idiomas[pessoasGrupo];
		for(int j = 0; j < pessoasGrupo; j++){
			cin >> idiomas[j];
		}
		cout << result(idiomas, pessoasGrupo) << endl;
	}
	return 0;
}
