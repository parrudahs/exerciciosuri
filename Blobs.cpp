#include <iostream>

using namespace std;

int main()
{
	int n;
	int somador = 0;
	float valor;
	cin >> n;
	for(int i = 0; i < n; i++){
		cin >> valor;
		while(valor > 1){
			somador++;
			valor /= 2;
		}
		cout << somador << " dias" << endl;
		somador = 0;			
	}
	return 0;
}
