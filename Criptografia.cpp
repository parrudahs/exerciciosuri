#include <iostream>
#include <algorithm>
#include <string>

using namespace std;

char letra(int numero)
{
	if((numero > 64 && numero < 91) || (numero > 96 && numero < 123))
		return numero + 3;
	return numero;
}

char deslocamento(int numero)
{
	return numero -1;
}

int main()
{
	int rodadas;
	string palavra;

	cin >> rodadas;
	cin.ignore();
	for(int i =0; i < rodadas; i++){

		getline(cin, palavra);

		for(int i = 0; i < palavra.size(); i++)
			palavra[i] = letra(palavra[i]);
		//cout << palavra << endl;

		reverse(palavra.begin(), palavra.end());
		//cout << palavra << endl;

		int metade = palavra.size() / 2;

		for(int i = metade; i <= palavra.size(); i++)
			palavra[i] = deslocamento(palavra[i]);
		cout << palavra << endl;
	}
	
	return 0;
}
