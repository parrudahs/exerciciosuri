#include <iostream>
#include <map>
#include <iomanip>
#include <string>

using namespace std;

double calcPorc(int valor, int total)
{
	return (valor * 100 * 1.0) / total;
}

int main()
{
	int l = 0;	
	int total = 0;
	string nome;

	cin >> l;
	
	map<string, int> arvores;
	map<string, int>::iterator it;

	l = l+1;

	while(true){
		getline(cin, nome);
		if(nome.size() == 0) {
			if(!arvores.empty()){				
				for(it = arvores.begin(); it != arvores.end(); it++)
					cout << it->first << " " << fixed << setprecision(4) << calcPorc(it->second,total) << endl;			
				if(l != 0) cout << endl;
				arvores.clear();
				total = 0;	
			}
			if(l <= 0)
				break;

			l--;		

		}else{
			arvores[nome]++;
			total++;
		}	
	}

	return 0;
}
