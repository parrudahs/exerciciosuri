#include <iostream>
#include <string>
#include <map>

using namespace std;

struct cargos{
	string nome;
	int  valor;
};

int main(){

	int m, n;
	int total = 0;
	int contador = 0;
	string descricao;	

	map<string, int> mapDesc;
	map<string, int>::iterator it;

	cin >> m >> n;

	struct cargos informacoes[m];

	//ler os cargos e valor
	for(int i = 0; i < m; i++){
		cin >> informacoes[i].nome >> informacoes[i].valor;	
	}
	
	while(contador < n){
		while(cin >> descricao){
			if(descricao == "."){
				contador++;
				total = 0;
				break;
			}	
			mapDesc[descricao]++;
		}
				
		for(int i = 0; i < m; i++){
			it = mapDesc.find(informacoes[i].nome);
			if(it != mapDesc.end())
				total += informacoes[i].valor * it->second;
		}
		mapDesc.clear();
		cout << total <<endl;
	}
		
	return 0;
}
