#include <iostream>
#include <string>

using namespace std;

int NumeroAlfabeto(char numero)
{
	return numero - 65;
}

int valorLinha(string palavra, int rodada)
{
	int valor = 0;
	for(int i = 0; i < palavra.length(); i++){
		valor +=  NumeroAlfabeto(palavra[i]) + rodada + i;
	}
	return valor;
}

int main()
{
	int casos = 0;
	int numeroRodada = 0;
	int total = 0;
	string palavra;

	cin >> casos;
	for(int i = 0; i < casos; i++){
		cin >> numeroRodada;
		for(int j=0; j< numeroRodada; j++)
		{
			cin >> palavra;
			total += valorLinha(palavra, j);
		}
		cout << total << endl;
		total = 0;	
	}
	return 0;
}
