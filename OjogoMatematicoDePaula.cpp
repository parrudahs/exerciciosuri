#include <iostream>
#include <string>

using namespace std;

int converteAscii(char valor){
	return valor;
}

int main()
{
	int testes;
	string letra;
	int a, b, result;

	cin >> testes;
		
	for(int i = 0; i < testes; i++){

		cin >> letra;

		a = (int) letra[2] - 48;
		b = (int) letra[0] - 48;

		if(converteAscii(letra[0]) == converteAscii(letra[2])){
			cout << a * b << endl;		
		}else if(converteAscii(letra[1]) > 64 && converteAscii(letra[1]) < 91 ){
			result = a - b;
			cout << result << endl;
		}else if(converteAscii(letra[1]) > 96 && converteAscii(letra[1]) < 220){
			result = a + b;
			cout << result << endl;
		}
	}
	return 0;
}
