#include <iostream>
#include <cmath>

using namespace std;

int numerador(int n1, int n2, int d1, int d2, char operador)
{
	switch(operador){
		case '+' :
			return (n1*d2) + (n2*d1);
		case '-' :
			return (n1*d2) - (n2*d1);
		case '*' :
			return n1*n2;
		case '/' :
			return n1*d2;
	}
	return 0;
}

int denominador(int n1, int n2, int d1, int d2, char operador)
{
	switch(operador){
		case '+' :
			return (d1*d2);
		case '-' :
			return (d1*d2);
		case '*' :
			return (d1*d2);
		case '/' :
			return n2*d1;
	}
	return 0;
}

int MDC(int m, int n)
{
	int r, x, y;
	x = m;
	y = n;
	do{
		r = x % y;
		x = y;
		y = r;
	}while(r != 0);
	return abs(x);
}


int main()
{
	int n1 = 0, n2 = 0, d1 = 0, d2 = 0;
	char operador;
	int nume = 0;
	int deno = 0;
	int casosTestes = 0;
	char barra1, barra2;

	cin >> casosTestes;

	while(casosTestes > 0 ){
		cin >> n1 >> barra1 >> d1 >> operador >> n2 >> barra2 >> d2 ;
		if(barra1 != '/' && barra2 != '/'){
			cout << "0/0 = 0/0" << endl;
			break;
		}
		nume = numerador(n1,n2,d1,d2,operador);
		deno = denominador(n1,n2,d1,d2,operador);
		cout << nume << "/" << deno << " = " << nume / MDC(nume, deno) << "/" << deno / MDC(nume, deno) << endl;
		casosTestes--;
	}
	
	return 0;
}
