#include <iostream>
#include <string>

using namespace std;

int main()
{
	string composicao;

	double soma = 0;
	int total = 0;

	while(cin >> composicao){
		if(composicao == "*")
			break;

		total = 0;	

		for(int i=0; i < composicao.size(); i++){
			
			if(composicao[i] == '/'){
				if(soma == 1){
					total += 1;
				}
				soma = 0;
			}else if(composicao[i] == 'W'){
				soma += 1;
			}else if(composicao[i] == 'H'){
				soma += 0.5;
			}else if(composicao[i] == 'Q'){
				soma += 0.25;
			}else if(composicao[i] == 'E'){
				soma += 0.125;
			}else if(composicao[i] == 'S'){
				soma += 0.0625;
			}else if(composicao[i] == 'T'){
				soma += 0.03125;
			}else if(composicao[i] == 'X'){
				soma += 0.015625;
			}
		}
	
		cout << total << endl;
	}
	return 0;
}
