#include <iostream>
#include <string>

using namespace std;

int QuantidadeProcessos(string letras, int processoMinimo)
{
	int numeroCiclo = 0;
	int n = 0;

	for(int i = 0; i< letras.length(); i++){
		if(letras[i] == 'W'){
			numeroCiclo++;
		}else if(letras[i] == 'R' && letras[i+1] != 'R' && n != processoMinimo ){
			numeroCiclo++;
			n = 0;
		}else {
			if(letras[i] == 'R' && letras[i+1] == 'R' && n != processoMinimo){
				n++;
			}
			if(n == processoMinimo){
				numeroCiclo++;
				n=0;
			}
		}
	}	
	return numeroCiclo;
}

int main(){

	string letras;
	int processoMinimo;

	while(true){
		cin >> letras;
		cin >> processoMinimo;
		if(!cin)
			break;
		cout << QuantidadeProcessos(letras, processoMinimo) << endl;
	}	
	return 0;
}












